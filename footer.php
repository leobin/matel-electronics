<?php do_action('sevenloft_footer_before_footer'); ?>

<footer class="footer">
    <div class="container">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-sm-6">
                        <div class="icon-box icon-box-side">
                            <i class="fas fa-truck"></i>
                            <div class="icon-box-content">
                                <h4 class="icon-box-title">Fast &amp; free shipping</h4>
                                <p>Orders $50 or more</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="icon-box icon-box-side">
                            <i class="fas fa-truck"></i>
                            <div class="icon-box-content">
                                <h4 class="icon-box-title">Customer support</h4>
                                <p>24/7 amazing services</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="icon-box icon-box-side">
                            <i class="fas fa-truck"></i>
                            <div class="icon-box-content">
                                <h4 class="icon-box-title">Fast &amp; free shipping</h4>
                                <p>Orders $50 or more</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="icon-box icon-box-side">
                            <i class="fas fa-truck"></i>
                            <div class="icon-box-content">
                                <h4 class="icon-box-title">Secured Payment</h4>
                                <p>When you sign up</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-middle">
            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <div class="widget widget-about">
                        <a href="<?php echo site_url(); ?>" class="logo-footer">
                            <?php if ( function_exists( 'the_custom_logo' ) ) {
                                if(the_custom_logo() != null)
                                {
                                    the_custom_logo();
                                }
                                else
                                {
                                    ?>
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/sevenloft.png" alt="<?php echo get_bloginfo('name') ?>" title="<?php echo get_bloginfo('name') ?>" >
                                    <?php
                                }
                            }
                            ?>
                        </a>
                        <div class="widget-body">
                            <p>Praesent dapibus, neque id cursus ucibus, <br>tortor neque egestas augue,
                                <br>eu vulputate magna eros eu erat.</p>

                            <div class="social-links">
                                <a href="#" class="social-link social-facebook fab fa-facebook-f"></a>
                                <a href="#" class="social-link social-twitter fab fa-twitter"></a>
                                <a href="#" class="social-link social-instagram fab fa-instagram"></a>
                                <a href="#" class="social-link fab fa-youtube"></a>
                                <a href="#" class="social-link fab fa-pinterest"></a>
                            </div>
                        </div>
                    </div>
                    <!-- End Widget -->
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="widget ml-lg-8">
                        <h4 class="widget-title">Useful Links</h4>
                        <ul class="widget-body">
                            <li>
                                <a href="#">About Us</a>
                            </li>
                            <li>
                                <a href="#">Order History</a>
                            </li>
                            <li>
                                <a href="#">FAQ</a>
                            </li>
                            <li>
                                <a href="contact-us.html">Contact Us</a>
                            </li>
                            <li>
                                <a href="#">Log in</a>
                            </li>
                        </ul>
                    </div>
                    <!-- End Widget -->
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="widget ml-lg-8">
                        <h4 class="widget-title">Customer Service</h4>
                        <ul class="widget-body">
                            <li>
                                <a href="#">Payment Methods</a>
                            </li>
                            <li>
                                <a href="#">Money-back guarantee!</a>
                            </li>
                            <li>
                                <a href="#">Returns</a>
                            </li>
                            <li>
                                <a href="#">Shipping</a>
                            </li>
                            <li>
                                <a href="#">Terms and conditions</a>
                            </li>
                            <li>
                                <a href="#">Privacy Policy</a>
                            </li>
                        </ul>
                    </div>
                    <!-- End Widget -->
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="widget ml-lg-8">
                        <h4 class="widget-title">My Account</h4>
                        <ul class="widget-body">
                            <li>
                                <a href="#">Sign in</a>
                            </li>
                            <li>
                                <a href="#">View Cart</a>
                            </li>
                            <li>
                                <a href="#">My Wishlist</a>
                            </li>
                            <li>
                                <a href="#">Track My Order</a>
                            </li>
                            <li>
                                <a href="#">Help</a>
                            </li>
                        </ul>
                    </div>
                    <!-- End Widget -->
                </div>
            </div>
        </div>
        <!-- End FooterMiddle -->
        <div class="footer-bottom">
            <div class="footer-left">
                <p class="copyright"><?php _e( '<a href="https://www.sevenloft.gr" title="Κατασκευη Ιστοσελιδων Θεσσαλονικη" target="_blank">Kατασκευή Ιστοσελίδας: SEVENLOFT</a>', 'sevenloft-eshop' ); ?></p>
            </div>
            <div class="footer-right">
                <figure class="payment">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/cards.png" alt="payment">
                </figure>
            </div>
        </div>
        <!-- End FooterBottom -->
    </div>
</footer>

<?php do_action('sevenloft_footer_after_footer'); ?>

</div> <!-- Offcanvas section end -->

	<button id="button" title="Go to top"><i class="fa fa-arrow-up" aria-hidden="true"></i></button>

<?php wp_footer(); ?>

</body>

</html>