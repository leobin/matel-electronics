<?php
/*Template Name: Home */
?>

<?php get_header(); ?>

<div class="main mt-4">
    <div class="page-content">
        <div class="container">
        <!-- End of Top Banner -->
            <section class="banners-grid pt-10 mt-3 pb-4">
                <h2 class="title title-simple">Special Offers</h2>
                <div class="row grid" style="position: relative; height: 454px;">
                    <div class="grid-item col-lg-6 height-x2" style="position: absolute; left: 0%; top: 0px;">
                        <div class="banner banner1 banner-fixed overlay-light appear-animate fadeInRightShorter appear-animation-visible">
                            <figure>
                                <img src="/demo/wp-content/uploads/2021/01/580x434.png" alt="banner image" width="580" height="434">
                            </figure>
                            <div class="banner-content top w-100 text-center">
                                <h4 class="banner-subtitle  font-weight-normal text-grey mb-4">
                                    Financing Offer</h4>
                                <h3 class="banner-title text-secondary  mb-2">Camera, Lens and Drone</h3>
                                <h5 class="font-secondary text-grey mb-0">Discount</h5>
                                <h3 class="text-primary lh-1">40% OFF</h3>
                            </div>
                            <div class="banner-content bottom w-100 text-center">
                                <a href="#" class="btn btn-dark">Shop Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item col-lg-3 col-xs-6 height-x1" style="position: absolute; left: 50%; top: 0px;">
                        <div class="banner banner2 banner-fixed overlay-light content-middle appear-animate fadeInDownShorter appear-animation-visible">
                            <figure>
                                <img src="/demo/wp-content/uploads/2021/01/280x207.png" alt="banner image" width="280" height="207">
                            </figure>
                            <div class="banner-content pl-4 pt-1">
                                <h5 class=" text-primary mb-1 ">Featured Event</h5>
                                <h4 class="banner-subtitle text-uppercase ls-s text-grey mb-0">Black Friday</h4>
                                <h3 class="banner-title text-secondary font-weight-bold ls-s">Sale</h3>
                                <a href="#" class="btn btn-link btn-dark btn-sm">Shop Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item col-lg-3 col-xs-6 height-x2" style="position: absolute; left: 75%; top: 0px;">
                        <div class="banner banner4 banner-fixed overlay-dark appear-animate fadeInLeftShorter appear-animation-visible">
                            <figure>
                                <img src="/demo/wp-content/uploads/2021/01/280x434.png" alt="banner image" width="280" height="434">
                            </figure>
                            <div class="banner-content top w-100 text-center">
                                <h5 class="banner-subtitle  font-weight-normal text-uppercase text-primary mb-1">
                                    Free Shipping</h5>
                                <h3 class="banner-title  text-white mb-2">Xbox One Wireless Controller</h3>
                                <h4 class="text-primary ">$37.00<del class="ml-2 text-white font-weight-normal ">$120.00</del></h4>
                            </div>
                            <div class="banner-content bottom w-100 text-center">
                                <a href="#" class="btn btn-outline btn-white">Shop Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item col-lg-3 col-xs-6 height-x1" style="position: absolute; left: 50%; top: 227px;">
                        <div class="banner banner3 banner-fixed overlay-dark content-middle appear-animate fadeInUpShorter appear-animation-visible" >
                            <figure>
                                <img src="/demo/wp-content/uploads/2021/01/280x207.png" alt="banner image" width="280" height="207">
                            </figure>
                            <div class="banner-content pl-4">
                                <h5 class=" text-white mb-0 ">New In</h5>
                                <h4 class="banner-subtitle text-uppercase ls-s text-white mb-2">Gopro</h4>
                                <h5 class="ls-s text-white mb-0">Discount</h5>
                                <h3 class="banner-title font-weight-bold text-white ls-s ">20% Off</h3>
                                <a href="#" class="btn btn-link btn-dark btn-sm">Shop Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="grid-space col-lg-3 col-xs-6" style="position: absolute; left: 0%; top: 454px;"></div>
                </div>
            </section>
        </div>
    </div>
</div>

<?php get_footer(); ?>