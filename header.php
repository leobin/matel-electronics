<!DOCTYPE html>

<html <?php language_attributes(); ?>>
<head>
	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<?php 
		global $wpdb;
		global $woocommerce;
		global $order;
		global $current_user;
	?>
    
	<?php wp_head(); ?>

</head>

<?php echo '<body class="'.join(' ',get_body_class()).'">'.PHP_EOL; ?>

<div class="overlay"></div>
<div class="overlayBasket"></div>

<div class="page-wrapper">	
	<header>
		<div class="container-fluid topheader">
			<div class="container">
				<div class="top-bar">
					<!-- <?php do_action('sevenloft_header_topheader'); ?> -->
					<span class="info-til">Τηλέφωνο παραγγελιών: <a href="tel:+302310555445">+30 2310 555 445</a></span>
					<span class="info-til"><a href="#">Εγγραφή στο B2B</a></span>
				</div>
			</div>
		</div>
		<div class="header-top">
			<div class="container">
				<div class="header-center">
					<p class="welcome-msg">3 items menu goes here</p>
				</div>
			</div>
		</div>
		
		<div class="header-middle">
			<div class="container">
				<div class="header-left">
					<a href="<?php echo site_url(); ?>" class="logo mr-4">
						<?php if ( function_exists( 'the_custom_logo' ) ) {
							if(the_custom_logo() != null)
							{
								the_custom_logo();
							}
							else
							{
								?>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/sevenloft.png" alt="<?php echo get_bloginfo('name') ?>" title="<?php echo get_bloginfo('name') ?>" >
								<?php
							}
						}
						?>
					</a>
				</div>
				<div class="header-center">
					<!-- End Logo -->
					<div class="header-search hs-expanded ml-2 ls-m">
						<?php echo do_shortcode('[wcas-search-form]'); ?>
					</div>
					<!-- End Header Search -->
				</div>
				<div class="header-right">
					<a class="call">
					<i class="fas fa-phone-alt"></i>
						<span class="font-weight-normal text-normal">Call Us Now:<strong class="d-block">0(800)
								123-456</strong></span>
					</a>
					<a class="login d-block" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>">
						<i class="far fa-user"></i>
					</a>
					<!-- End Login -->
					<a class="wishlist" href="#">
					<i class="fas fa-heart"></i>
					</a>
					<!-- End Wishlist -->

					<div class="dropdown cart-dropdown type2">
						<a href="<?php echo wc_get_cart_url(); ?>" class="cart-toggle label-down link">
							<i class="fas fa-shopping-bag"></i><span class="cart-count">3</span></i>
						</a>
                            <!-- End Cart Toggle -->
						<div class="dropdown-box">
							<div class="product product-cart-header">
								<span class="product-cart-counts">2 items</span>
								<span><a href="cart.html">View cart</a></span>
							</div>
							<div class="products scrollable">
								<div class="product product-cart">
									<div class="product-detail">
										<a href="product.html" class="product-name">Solid Pattern In Fashion Summer Dress</a>
										<div class="price-box">
											<span class="product-quantity">1</span>
											<span class="product-price">$129.00</span>
										</div>
									</div>
									<figure class="product-media">
										<a href="#">
											<img src="images/cart/product-1.jpg" alt="product" width="90" height="90">
										</a>
										<button class="btn btn-link btn-close">
											<i class="fas fa-times"></i>
										</button>
									</figure>
								</div>
								<!-- End of Cart Product -->
								<div class="product product-cart">
									<div class="product-detail">
										<a href="product.html" class="product-name">Mackintosh Poket Backpack</a>
										<div class="price-box">
											<span class="product-quantity">1</span>
											<span class="product-price">$98.00</span>
										</div>
									</div>
									<figure class="product-media">
										<a href="#">
											<img src="images/cart/product-2.jpg" alt="product" width="90" height="90">
										</a>
										<button class="btn btn-link btn-close">
											<i class="fas fa-times"></i>
										</button>
									</figure>
								</div>
								<!-- End of Cart Product -->
							</div>
							<!-- End of Products  -->
							<div class="cart-total">
								<label>Subtotal:</label>
								<span class="price">$42.00</span>
							</div>
							<!-- End of Cart Total -->
							<div class="cart-action">
								<a href="checkout.html" class="btn btn-dark"><span>Checkout</span></a>
							</div>
							<!-- End of Cart Action -->
						</div>
						<!-- End Dropdown Box -->
					</div>
				</div>
			</div>
		</div>

		<div class="sticky-content-wrapper" >
			<div class="header-bottom sticky-header fix-top sticky-content">
                <div class="container">
                    <div class="row gutter-no scrollable scrollable-light bg-secondary text-white w-100">
                        <div class="category category-icon">
                            <a href="demo29-shop.html">
                                <figure class="category-media">
								<i class="fas fa-broadcast-tower"></i>
                                </figure>
                                <div class="category-content">
                                    <h4 class="category-name">Computers</h4>
                                </div>
                            </a>
                        </div>
                        <div class="category category-icon">
                            <a href="demo29-shop.html">
                                <figure class="category-media">
								<i class="fas fa-broadcast-tower"></i>
                                </figure>
                                <div class="category-content">
                                    <h4 class="category-name">Component</h4>
                                </div>
                            </a>
                        </div>
                        <div class="category category-icon">
                            <a href="demo29-shop.html">
                                <figure class="category-media">
								<i class="fas fa-broadcast-tower"></i>
                                </figure>
                                <div class="category-content">
                                    <h4 class="category-name">Electronics</h4>
                                </div>
                            </a>
                        </div>
                        <div class="category category-icon">
                            <a href="demo29-shop.html">
                                <figure class="category-media">
								<i class="fas fa-broadcast-tower"></i>
                                </figure>
                                <div class="category-content">
                                    <h4 class="category-name">Game consoles</h4>
                                </div>
                            </a>
                        </div>
                        <div class="category category-icon">
                            <a href="demo29-shop.html">
                                <figure class="category-media">
								<i class="fas fa-broadcast-tower"></i>
                                </figure>
                                <div class="category-content">
                                    <h4 class="category-name">Networks</h4>
                                </div>
                            </a>
                        </div>
                        <div class="category category-icon">
                            <a href="demo29-shop.html">
                                <figure class="category-media">
								<i class="fas fa-broadcast-tower"></i>
                                </figure>
                                <div class="category-content">
                                    <h4 class="category-name">Office Solution</h4>
                                </div>
                            </a>
                        </div>
                        <div class="category category-icon">
                            <a href="demo29-shop.html">
                                <figure class="category-media">
								<i class="fas fa-broadcast-tower"></i>
                                </figure>
                                <div class="category-content">
                                    <h4 class="category-name">SmartPhone</h4>
                                </div>
                            </a>
                        </div>
                        <div class="category category-icon">
                            <a href="demo29-shop.html">
                                <figure class="category-media">
								<i class="fas fa-broadcast-tower"></i>
                                </figure>
                                <div class="category-content">
                                    <h4 class="category-name">Industrial</h4>
                                </div>
                            </a>
                        </div>
                        <div class="category category-icon">
                            <a href="demo29-shop.html">
                                <figure class="category-media">
								<i class="fas fa-broadcast-tower"></i>
                                </figure>
                                <div class="category-content">
                                    <h4 class="category-name">Headphones</h4>
                                </div>
                            </a>
                        </div>
                        <div class="category category-icon">
                            <a href="demo29-shop.html">
                                <figure class="category-media">
								<i class="fas fa-broadcast-tower"></i>
                                </figure>
                                <div class="category-content">
                                    <h4 class="category-name">Accessories</h4>
                                </div>
                            </a>
                        </div>
                        <div class="category category-icon">
                            <a href="demo29-shop.html">
                                <figure class="category-media">
								<i class="fas fa-broadcast-tower"></i>
                                </figure>
                                <div class="category-content">
                                    <h4 class="category-name">All Categories</h4>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</header>
